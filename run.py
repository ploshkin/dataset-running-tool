import sys
import timeit
from template_estimators import distorted_titanic_estimator


def main():
    start_time = timeit.default_timer()
    try:
        distorted_titanic_estimator.run()
    except Exception as e:
        print('{error}\n'.format(error=e), file=sys.stderr)

    duration = timeit.default_timer() - start_time
    print('{duration}(s) spent\n'.format(duration=duration))


if __name__ == '__main__':
    main()
