import os
import re
import sys
import datetime
import subprocess
import shutil
import pandas as pd
from collections import namedtuple


def create_avs(*, path_to_avs, path_to_video):
    ext = path_to_video.split('.')[-1]
    assert(ext in {'avi', 'mkv'}), 'Not supported extension: {ext}'.format(ext=ext)

    if ext == 'avi':
        opener = 'avisource("{video}")'
    elif ext == 'mkv':
        opener = 'ffvideosource("{video}")'

    avs_src = 'SetMemoryMax(128)\n' +\
              opener.format(video=path_to_video) +\
              '.Lanczos4Resize(960, 544).ConvertToRGB24()\n'
    with open(path_to_avs, 'w') as avs:
        avs.write(avs_src)


def parse_res_file(*, path_to_res_file, template=r"(?P<end_frame>\d+)\t(?P<value>[+-]?\d+.\d+)\n"):
    results = []
    with open(path_to_res_file) as res_file:
        for line in res_file.readlines():
            res = namedtuple('Res', ['end_frame', 'value'])
            m = re.match(template, line)
            res.end_frame = int(m.group('end_frame'))
            res.value = float(m.group('value'))
    return results


class Worker(object):
    def __init__(self, exe, cwd):
        self._exe = exe
        self._cwd = cwd

    def run(self, *args):
        subprocess.call([self._exe, *args], cwd=self._cwd)        


class Runner(object):
    def __init__(self, *, folder, file_info, index_col, metrics_name, output_dir, vqmt_exe,
                 output_template=r'(?P<frame>\d+)\t(?P<value>[+-]?\d+.\d+)\n',
                 result_folder=os.path.curdir):
        self.ds_folder = folder
        self.ds_info = pd.read_csv(file_info, index_col=index_col)
        self._metrics_name = metrics_name
        self._output_dir = output_dir
        self._vqmt_exe = vqmt_exe
        self._output_template = output_template
        self.result_table = pd.DataFrame(columns=[index_col, 'end_frame', metrics_name]).\
            set_index(index_col)
        self.path_to_result_table = os.path.join(result_folder,
                                                 'results_{:%Y-%b-%d-%H-%M}.csv'.format(datetime.datetime.now()))

    def _run(self, *, left_avs, right_avs):
        worker = Worker(exe=self._vqmt_exe, cwd=os.path.curdir)
        worker.run(
            '-o={output_dir}'.format(output_dir=self._output_dir),
            '-m={metrics}'.format(metrics=self._metrics_name),
            left_avs,
            right_avs
        )

    def run(self):
        if not os.path.isdir(self._output_dir):
            os.mkdir(self._output_dir)
        try:
            for video, info in self.ds_info.iterrows():
                print(video)
                left_avs = os.path.join(self._output_dir, 'left.avs')
                right_avs = os.path.join(self._output_dir, 'right.avs')
                create_avs(path_to_video=os.path.join(self.ds_folder, video + '_left.avi'),
                           path_to_avs=left_avs)
                create_avs(path_to_video=os.path.join(self.ds_folder, video + '_right.avi'),
                           path_to_avs=right_avs)

                self._run(left_avs=left_avs, right_avs=right_avs)

                path_to_res_file = os.path.join(self._output_dir, 
                                                '{metrics}.txt'.format(metrics=self._metrics_name))

                results = parse_res_file(
                    path_to_res_file=path_to_res_file,
                    template=self._output_template
                )
                for res in results:
                    self.result_table.loc[video] = pd.Series({'end_frame': res.end_frame,
                                                              'temporal_shift': res.value})

                os.remove(path_to_res_file)
        except Exception as e:
            print('{error}'.format(error=e), file=sys.stderr)

        self.result_table.to_csv(self.path_to_result_table)
        shutil.rmtree(self._output_dir)
