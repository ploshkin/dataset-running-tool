from runner import Runner


distorted_titanic_estimator = Runner(
    folder=r'D:\study\videogroup\temporal_shift\datasets\titanic_set_distorted\src',
    file_info=r'D:\study\videogroup\temporal_shift\datasets\titanic_set_distorted\seqs_list_titanic.csv',
    index_col='name',
    metrics_name='temporal_shift',
    output_dir=r'D:\study\videogroup\temporal_shift\runs\tmp',
    vqmt_exe=r'D:\Study\videogroup\vg_main\build\VQMT3D\bin\VQMT3D.exe',
    output_template=r'(?P<end_frame>\d+) (?P<value>[+-]?\d+.\d+)\n',
    result_folder=r'D:\study\videogroup\temporal_shift\runs\titanic_set_distorted'
)

original_titanic_estimator = Runner(
    folder=r'D:\study\videogroup\temporal_shift\datasets\titanic_set\src',
    file_info=r'D:\study\videogroup\temporal_shift\datasets\titanic_set\seqs_list_titanic.csv',
    index_col='name',
    metrics_name='temporal_shift',
    output_dir=r'D:\study\videogroup\temporal_shift\runs\tmp',
    vqmt_exe=r'D:\Study\videogroup\vg_main\build\VQMT3D\bin\VQMT3D.exe',
    output_template=r'(?P<end_frame>\d+) (?P<value>[+-]?\d+.\d+)\n',
    result_folder=r'D:\study\videogroup\temporal_shift\runs\titanic_set'
)
